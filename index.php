<?php

/*
 * Plugin Name: GPF Hidden Posts
 * Description: Pages non indexées
 */



if (!is_admin()) {

    add_action('pre_get_posts', function($query) {

        if ($query->is_singular)
            return;

        //$meta_query = $query->meta_query;
        $meta_query = $query->query['meta_query'] ?? [];

        if (!is_array($meta_query)) {
            $meta_query = [];
        }


        $meta = '';

        if ($query->is_search) {
            $meta = 'discretion_search';
        }

        if ($query->is_archive) {
            $meta = 'discretion_related';
        }

        if ($query->is_feed) {
            $meta = 'discretion_syndication';
        }


        $hidden_query = [
            [
                'key' => 'hidden_post',
                'compare' => 'NOT EXISTS',
            ]
        ];


        if ($meta !== "") {
            $hidden_query['relation'] = 'AND';

            $hidden_query[] = [
                'key' => $meta,
                'compare' => 'NOT EXISTS',
            ];
        }

        if ( empty($meta_query) ) {
            $meta_query = $hidden_query;
        }
        else {
            $meta_query[] = $hidden_query;
            $meta_query['relation'] = 'AND';
        }


        $query->set('meta_query', $meta_query);

    });
}


add_action('add_meta_boxes', function($post_type, $post) {
    add_meta_box(
        'discretion',
        "Discrétion",
        'render_discretion_box',
        null,
        'side',
        'high',

    );
}, 10, 2);




add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function() {
    global $wpdb;
    return $wpdb->get_col( "SELECT post_id FROM gp_postmeta where meta_key = 'discretion_syndication' and meta_value = '1'" );

});



function render_discretion_box( $post ) {
    $hidden_post = get_post_meta($post->ID, 'hidden_post', true);

    $search = get_post_meta($post->ID, 'discretion_search', true);
    $related = get_post_meta($post->ID, 'discretion_related', true);
    $syndication = get_post_meta($post->ID, 'discretion_syndication', true);
    $tracking = get_post_meta($post->ID, 'discretion_tracking', true);


    if ( ! empty($hidden_post) ) {
        $search = 'checked';
        $related = 'checked';
        $syndication = 'checked';
        $tracking = "";
    }
    else {
        $search = $search ? "checked" : "";
        $related = $related ? "checked" : "";
        $syndication = $syndication ? "checked" : "";
        $tracking = $tracking ? "checked" : "";
    }


    echo <<< END
    <div class="always-visible">
        <div class="form-group">
            <div class="checkbox">
                <label><input type="checkbox" name="discretion_search" value="1" $search/> Ne pas afficher dans le moteur de recherche</label>
            </div>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label><input type="checkbox" name="discretion_related" value="1" $related/> Ne pas afficher dans les zones "Pages liées" et autres listings</label>
            </div>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label><input type="checkbox" name="discretion_syndication" value="1" $syndication/> Exclure des RSS et Sitemap</label>
            </div>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label><input type="checkbox" name="discretion_tracking" value="1" $tracking/> Ne pas inclure le tracking Analytics et marketing</label>
            </div>
        </div>
    </div>
    END;

}



/*
add_action('post_submitbox_misc_actions', function($post) {
    $value = get_post_meta($post->ID, 'hidden_post', true);

    echo  '<div class="misc-pub-section misc-pub-section-last">';
    echo '<label><input type="checkbox"' . (!empty($value) ? ' checked="checked" ' : null) . ' value="1" name="hidden_post" /> Page cachée (si coché)</label>';
    echo '</div>';

});
*/

add_action('save_post', function($post_id, $post, $update) {
    if (!count($_POST))
		return;

	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

	if(!current_user_can('edit_page', $post_id))
        return;

    if ( wp_is_post_revision( $post_id ) ) {
        return;
    }


    $metas = ['search', 'related', 'syndication', 'tracking'];

    foreach ($metas as $meta) {
        if (isset($_POST['discretion_' . $meta]) && $_POST['discretion_' . $meta] === '1') {
            update_post_meta($post_id, 'discretion_' . $meta, '1');
        }
        else {
            delete_post_meta($post_id, 'discretion_' . $meta);
        }
    }

    delete_post_meta($post_id, 'hidden_post');



}, 1, 3); // haute priorité, on veut pouvoir être executé avant le hook du plugin Fantastic Elasticsearch (qui est à 10)
